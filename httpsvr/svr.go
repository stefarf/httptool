package httpsvr

import (
	"bitbucket.org/stefarf/iferr"
	"crypto/tls"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"
)

func ListenAndServe(addr, cerFile, keyFile string, timeout time.Duration, handler http.Handler) error {
	if os.Getenv("LISTEN_PID") == strconv.Itoa(os.Getpid()) {
		return systemdServer(cerFile, keyFile, timeout, handler)
	} else {
		return nosystemdServer(addr, cerFile, keyFile, timeout, handler)
	}
}

func nosystemdServer(addr, cerFile, keyFile string, timeout time.Duration, handler http.Handler) error {
	srv := &http.Server{
		Addr:         addr,
		Handler:      handler,
		ReadTimeout:  timeout,
		WriteTimeout: timeout,
	}
	if cerFile != "" && keyFile != "" {
		return srv.ListenAndServeTLS(cerFile, keyFile)
	} else {
		return srv.ListenAndServe()
	}

}

func systemdServer(cerFile, keyFile string, timeout time.Duration, handler http.Handler) error {
	// Server setting
	srv := &http.Server{
		Handler:      handler,
		ReadTimeout:  timeout,
		WriteTimeout: timeout,
	}

	// Listener from systemd
	systemdListener, err := net.FileListener(os.NewFile(3, "from systemd"))
	iferr.Exit(err, "")

	var lsnr net.Listener
	if cerFile != "" && keyFile != "" {

		// TLS config
		tlsConfig := &tls.Config{
			Certificates:             make([]tls.Certificate, 1),
			NextProtos:               []string{"h2", "http/1.1"},
			PreferServerCipherSuites: true,
		}
		tlsConfig.Certificates[0], err = tls.LoadX509KeyPair(cerFile, keyFile)
		iferr.Exit(err, "")

		// Set listener
		lsnr = tls.NewListener(systemdListener, tlsConfig)

	} else {

		// Set listener
		lsnr = systemdListener

	}
	return srv.Serve(lsnr)
}
