package httpmdw

import (
	"fmt"
	"net/http"
	"sync"
	"time"
)

func RequestSizeLimit(size int64) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				r.Body = http.MaxBytesReader(w, r.Body, size)
				next.ServeHTTP(w, r)
			})
	}
}

func OpenLimit(limit int) func(http.Handler) http.Handler {
	if limit < 0 {
		limit = 0
	}
	var lck struct {
		sync.Mutex
		cnt int
	}

	return func(next http.Handler) http.Handler {

		return http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {

				// Increment number of connection
				lck.Lock()
				if lck.cnt >= limit {
					lck.Unlock()
					http.Error(w, "Too many opened connection", http.StatusTooManyRequests)
					return
				}
				lck.cnt++
				lck.Unlock()

				// Next
				next.ServeHTTP(w, r)

				// Decrement number of connection
				lck.Lock()
				lck.cnt--
				lck.Unlock()

			})

	}
}

func TPSLimit(limit, burst float64) func(http.Handler) http.Handler {
	if limit != 0 && limit < 1 {
		limit = 5
	}
	if burst < 1 {
		burst = 5
	}

	var lck struct {
		sync.Mutex
		tx   float64
		prev time.Time
	}
	lck.prev = time.Now()

	return func(next http.Handler) http.Handler {

		return http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {

				lck.Lock()

				// Calculate available tx counter
				now := time.Now()
				secs := now.Sub(lck.prev).Seconds()
				lck.tx += secs * limit
				if lck.tx > burst {
					lck.tx = burst
				}
				lck.prev = now

				if lck.tx < 1 {
					// Not enough counter, error
					lck.Unlock()
					http.Error(w, "Request denied due to limited TPS", http.StatusTooManyRequests)
					return
				}

				// Consume the counter
				lck.tx--
				lck.Unlock()

				// Next
				next.ServeHTTP(w, r)

			})

	}
}

func Log(next http.Handler) http.Handler {
	var cnt uint
	var lck sync.Mutex

	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			lck.Lock()
			now := cnt
			cnt++
			lck.Unlock()

			fmt.Printf("[%d] Method: %s\n", now, r.Method)
			fmt.Printf("[%d] Path: %s\n", now, r.URL.Path)
			for key, value := range r.Header {
				fmt.Printf("[%d] Header: %s => %v\n", now, key, value)
			}

			next.ServeHTTP(w, r)
		})
}
