package restsvr

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type (
	Handler func(ctx *Context)
)

func By(h Handler) func(res http.ResponseWriter, req *http.Request, par httprouter.Params) {
	return func(res http.ResponseWriter, req *http.Request, par httprouter.Params) {
		ctx := &Context{res: res, req: req, par: par}
		h(ctx)
	}
}
