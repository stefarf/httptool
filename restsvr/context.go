package restsvr

import (
	"bitbucket.org/stefarf/iferr"
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"net/http"
	"strconv"
)

type Context struct {
	res http.ResponseWriter
	req *http.Request
	par httprouter.Params
}

func (ctx *Context) StatusInternalServerError(msg string) {
	http.Error(ctx.res, msg, http.StatusInternalServerError)
}
func (ctx *Context) StatusBadRequest(msg string)   { http.Error(ctx.res, msg, http.StatusBadRequest) }
func (ctx *Context) StatusUnAuthorized(msg string) { http.Error(ctx.res, msg, http.StatusUnauthorized) }
func (ctx *Context) RedirectSeeOther(url string) {
	http.Redirect(ctx.res, ctx.req, url, http.StatusSeeOther)
}
func (ctx *Context) Header(name string) string { return ctx.req.Header.Get(name) }
func (ctx *Context) Read() ([]byte, error)     { return ioutil.ReadAll(ctx.req.Body) }

func (ctx *Context) ReadString() (string, error) {
	b, err := ctx.Read()
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func (ctx *Context) ReadJson(v interface{}) error {
	b, err := ctx.Read()
	if err != nil {
		return err
	}
	return json.Unmarshal(b, v)
}

func (ctx *Context) Write(b []byte, contentType string) error {
	if contentType != "" {
		ctx.res.Header().Set("Content-Type", contentType)
	}
	_, err := ctx.res.Write(b)
	return err
}

func (ctx *Context) WriteString(s string) error { return ctx.Write([]byte(s), "text/plain") }

func (ctx *Context) WriteJson(v interface{}) error {
	b, err := json.Marshal(v)
	if err != nil {
		return err
	}
	return ctx.Write(b, "application/json")
}

func (ctx *Context) Param(name string) string { return ctx.par.ByName(name) }

func (ctx *Context) ParamInt(name string) (int, error) {
	s := ctx.par.ByName(name)
	return strconv.Atoi(s)
}

func (ctx *Context) ParseForm() map[string][]string {
	iferr.Panic(ctx.req.ParseForm())
	return ctx.req.Form
}
