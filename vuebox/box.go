package vuebox

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"path"
)

func Box(dir string, mux *http.ServeMux) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatalf("error read dir '%s'", dir)
	}
	for _, f := range files {
		// Skip index.html, it will be the last handler
		// Ignore report.html => webpack bundle analyzer
		if f.Name() == "index.html" ||
			f.Name() == "report.html" {
			continue
		}

		full := path.Join(dir, f.Name())
		if f.IsDir() {
			strip := "/" + f.Name()
			handle := strip + "/"
			mux.Handle(handle, http.StripPrefix(strip, http.FileServer(http.Dir(full))))
			fmt.Printf("vuebox: handle '%s', strip '%s', file server for directory '%s'\n", handle, strip, full)
		} else {
			handle := "/" + f.Name()
			mux.Handle(handle, fileHandler(full))
			fmt.Printf("vuebox: handle '%s', file handler '%s'\n", handle, full)
		}
	}
	handle := "/"
	full := path.Join(dir, "index.html")
	mux.Handle(handle, fileHandler(full))
	fmt.Printf("vuebox: handle '%s', file handler '%s'\n", handle, full)
}

func fileHandler(fileName string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, fileName)
	})
}
