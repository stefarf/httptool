Collection of http related go packages:
- httpmdw: HTTP middlewares.
- httpsvr: HTTP server helper.
- restcli: REST client helper.
- restsvr: REST server helper.
- vuebox:: Vue app server.
