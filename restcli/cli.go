package restcli

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

type Response struct {
	Bytes      []byte
	StatusCode int
	Response   *http.Response
}

func (res *Response) JsonUnmarshal(v interface{}) error { return json.Unmarshal(res.Bytes, v) }

func GET(
	url string,
	timeout time.Duration,
	header map[string][]string,
	body []byte,
) (*Response, error) {
	return Method("GET", url, timeout, header, body)
}

func POST(
	url string,
	timeout time.Duration,
	header map[string][]string,
	body []byte,
) (*Response, error) {
	return Method("POST", url, timeout, header, body)
}

func PUT(
	url string,
	timeout time.Duration,
	header map[string][]string,
	body []byte,
) (*Response, error) {
	return Method("PUT", url, timeout, header, body)
}

func Method(
	method, url string,
	timeout time.Duration,
	header map[string][]string,
	body []byte,
) (*Response, error) {

	// Create new request
	req, err := http.NewRequest(method, url, bytes.NewReader(body))
	if err != nil {
		return nil, err
	}

	// Set header
	for key, vals := range header {
		for _, val := range vals {
			req.Header.Add(key, val)
		}
	}

	// Request
	res, err := (&http.Client{Timeout: timeout}).Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	// Read response
	cliRes := &Response{StatusCode: res.StatusCode, Response: res}
	cliRes.Bytes, err = ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	return cliRes, nil
}
